package spark_streaming_with_kafka

import net.liftweb.json.Extraction.decompose
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.spark.sql.{DataFrame, ForeachWriter, Row, SparkSession}
import org.apache.spark.sql.functions.{col, from_json}
import org.apache.spark.sql.streaming.{DataStreamWriter, StreamingQuery, Trigger}
import org.apache.spark.sql.types.{IntegerType, StringType, StructType}

import java.util.Properties

import net.liftweb.json._

import scala.concurrent.duration

object App {
  def main(args: Array[String]): Unit = {

    val schema: StructType = new StructType()
      .add("id", IntegerType)
      .add("firstname", StringType)
      .add("middlename", StringType)
      .add("lastname", StringType)
      .add("dob_year", IntegerType)
      .add("dob_month", IntegerType)
      .add("gender", StringType)
      .add("salary", IntegerType)

    val spark: SparkSession = SparkSession.builder()
      .master("local[1]")
      .appName("SparkByExample")
      .getOrCreate()

    val df: DataFrame = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "192.168.1.196:9092")
      .option("subscribe", "change")
      .option("failOnDataLoss", "false")
      .load()

    val streamingQuery: StreamingQuery = {
      val personDf: DataFrame = df.selectExpr("CAST(value AS STRING)")
        .select(from_json(col("value"), schema).as("data"))
        .select("data.*")

      val streamWriter: DataStreamWriter[Row] = personDf.writeStream.foreach {
        new ForeachWriter[Row] {
          private var partitionId = -1L
          private var epochId = -1L

          override def open(partitionId: Long, epochId: Long): Boolean = {
            this.epochId = epochId
            this.partitionId = partitionId
            true
          }

          override def process(value: Row): Unit = {
            processEvent(value)
          }

          override def close(errorOrNull: Throwable): Unit = {
            if(Option(errorOrNull).isDefined) print(errorOrNull.getMessage)
          }
        }
      }

      streamWriter
        .option("checkpointLocation", "checkpoints")
        .trigger(Trigger.Continuous(duration.DurationInt(30).seconds))
        .start()
    }

    streamingQuery.awaitTermination()

  }

  private def processEvent(value: Row): Unit = {
    implicit val formats: DefaultFormats.type = DefaultFormats

    val personOne = parse(value.json).extract[PersonOne]

    val c = prettyRender(decompose(personOne)).getBytes()
    writeDataToQueue("resulthere", personOne.id, c)
  }

  private def writeDataToQueue(topic: String, key: Int, value: Array[Byte]): Unit = {
    val properties = new Properties()
    properties.put("bootstrap.servers", "127.0.0.1:9092")
    properties.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer")
    properties.put("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer")

    val producer = new KafkaProducer[Int, Array[Byte]](properties)
    val record = new ProducerRecord[Int, Array[Byte]](
      topic,
      key,
      value
    )
    producer.send(record)
    producer.close()
  }
}

case class PersonOne(var id: Int, var firstname: String)