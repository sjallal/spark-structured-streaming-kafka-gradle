## Create a gradle project:
```shell
$ mkdir demo
$ cd demo
$ gradle init

Select type of project to generate:
  1: basic
  2: application
  3: library
  4: Gradle plugin
Enter selection (default: basic) [1..4] 2

Select implementation language:
  1: C++
  2: Groovy
  3: Java
  4: Kotlin
  5: Scala
  6: Swift
Enter selection (default: Java) [1..6] 5

Select build script DSL:
  1: Groovy
  2: Kotlin
Enter selection (default: Groovy) [1..2] 1

Project name (default: demo):
Source package (default: demo):


BUILD SUCCESSFUL
2 actionable tasks: 2 executed
```


## Start Spark Application:
### Download Spark:

**Link:** https://spark.apache.org/downloads.html

**Spark Release:** *3.4.1*

**Package Type:** *Pre-built for Apache Hadoop 3.3 and later*
```shell
$ cd spark-3.4.1-bin-hadoop3-scala2.13
$ cd conf
$ cp spark-env.sh.template spark-env.sh
$ vi spark-env.sh # Update java path
$ sbin/start-all.sh # Spin up spark Master and Worker
$ jps # Check if Master and Worker are running
$ kill -9 \`jps | grep -i "Master" | awk '{print $1}'\` # Kill Master Node.
$ kill -9 \`jps | grep -i "Worker" | awk '{print $1}'\` # Kill Worker Node.
$ ~/spark-3.4.1-bin-hadoop3-scala2.13
        ./bin/spark-submit --packages org.apache.spark:spark-sql-kafka-0-10_2.13:3.4.0,net.liftweb:lift-json_2.13:3.4.1 --class spark_streaming_with_kafka.App /Users/saqibjallal/Learnings/learning_spark_2.0/spark_streaming_with_kafka/app/build/libs/app.jar
```

